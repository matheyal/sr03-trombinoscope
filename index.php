<html>
<head>
	<title>Trombinoscope UTC</title>
	<meta charset="UTF-8">

	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0-beta1/jquery.js"></script>
	<script src="js/trombi.js"></script>

	<link rel="stylesheet" href="css/foundation.css" />
  <link rel="stylesheet" href="css/app.css" />
  <link rel="stylesheet" href="css/trombi.css" />
</head>
<body>
	<div id="top">
		<div id="top-content">
			<h1 id="main-title">Trombinoscope<br>UTC</h1>
			<div id="forms" class="row">

				<form id="formNomPrenom" name="formNomPrenom" method="post" action="" onsubmit="updateResultsNom(); return false;" class="large-6 columns">
					<p><input class="formElement" id="prenom" type="text" name="prenom" placeholder="Prénom" <?php if(isset($_GET['prenom'])){echo "value='".$_GET['prenom']."'";}?> autofocus/></p>
					<p><input class="formElement" id="nom" type="text" name="nom" placeholder="Nom" <?php if(isset($_GET['nom'])){echo "value='".$_GET['nom']."'";}?>/></p>
					<p><input id="submitNomPrenom" class="formSubmit" type="submit" value="Rechercher par nom" disabled="true" /></p>
				</form>

				<form id="formStructure" name="formStructure" method="post" action="" onsubmit="updateResultsStructure(); return false;" class="large-6 columns">
					<p>
						<select class="formElement" id="structure" name="structure" >
							<option class="formElement" disabled selected hidden value=''>Structure</option>
							<option class="formElement" value=''>--</option>
							<?php include('getStructures.php') ?>
						</select>
					</p>
					<p>
						<select class="formElement" id="sousStructure" name="sousStructure" disabled>
							<option class="formElement" disabled selected hidden value=0>Sous-structure</option>
							<option class="formElement" value=0>--</option>
						</select>
					</p>
					<p><input id="submitStructure" class="formSubmit" type="submit" value="Rechercher par structure" disabled="true" /></p>
				</form>

			</div>
		</div>
	</div>

	<div id="results"></div>

</body>
</html>
