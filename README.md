# SR03 - Projet n°1 - Trombinoscope

## Auteurs
Alexis MATHEY & Idir RAHIL

## Description
Trombinoscope de l'UTC développé dans le cadre d'un projet de l'UV SR03 à l'UTC.

## Sujet
Le but de ce projet est de faire un trombinoscope, pour cela vous devrez :
* Créer une page web Dynamique qui contiendra un formulaire avec comme champs prénom nom et qui affichera le résultat de votre recherche.
* Dans votre formulaire, il faut contrôler qu'au moins un des deux champs soit rempli ( JAVASCRIPT ).
* Vous devrez appeler un webservice de type REST qui vous retournera le résultat en JSON.
* Avant d'appeler le webservice, vous devrez tester si il est disponible grâce au requête HTTP.
* Pour la gestion de votre formulaire, vous devrez utiliser le langage PHP ou JAVA EE.
* Votre trombinoscope devra être ergonomique et responsive design ( CSS ).

## Technologies utilisées
* HTML
* PHP (traitement des JSON retournés par les webservices)
* Javascript/JQuery (affichage dynamique des résultats, contrôle de formulaires, "smooth scroll")
* Foundation (front-end responsive design)
