<?php
  $structure = $_POST['structure'];

  $query = "https://webapplis.utc.fr/Trombi_ws/mytrombi/structfils?lid=".$structure;

  set_error_handler(function() { echo "<p>Webservice indisponible</p>";});
  $json = file_get_contents($query);
  restore_error_handler();

  $res = json_decode($json, true);

  for($i = 0 ; $i < count($res) ; $i++){
    $nomStruct = $res[$i]['structureLibelle'];
    $numStruct = $res[$i]['structure']['structId'];
    echo "<option class=\"formElement\" value=\"".$numStruct."\">".$nomStruct."</option>";
  }
?>
