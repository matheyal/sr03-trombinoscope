// Egalise les hauteurs des blocs du groupe passé en argument
function equalHeight(group) {
  var tallest = 0;
  group.each(function() {
    thisHeight = $(this).height();
    if(thisHeight > tallest) {
      tallest = thisHeight;
    }
  });
  group.height(tallest);
}

// Met à jour les choix disponibles dans la liste déroulante "sousStructure" lorsqu'une struture est sélectionnée
function updateSousStructures(){
  var http = new XMLHttpRequest();
  http.open("POST", "getSousStructures.php", true);
  http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
  var params = "structure=" + document.getElementById("structure").value;
  http.send(params);
  // Une fois le résultat de la requête obtenu, on introduit le code reçu
  // (qui représente toutes les sous-structures de la structure sélectionnée)
  // entre les balises du formulaire "sousStructure"
  http.onload = function() {
      document.getElementById("sousStructure").innerHTML += http.responseText;
  }
}

// Charge les résultats d'une recherche par nom avec rechercheNom.php puis les insère dans le div "results"
function updateResultsNom(){
  document.getElementById("results").innerHTML = '';
  var http = new XMLHttpRequest();
  http.open("POST", "rechercheNom.php", true);
  http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
  var params = "nom=" + document.getElementById("nom").value + "&prenom=" + document.getElementById("prenom").value;
  http.send(params);
  // Une fois le résultat de la requête obtenu, on introduit le code reçu
  // (qui représente toutes les personnes correspondant aux critères)
  // entre les balises du div "results"
  http.onload = function() {
		if(http.responseText != ''){
    	document.getElementById("results").innerHTML = http.responseText;
  	}
  	else{
      document.getElementById("prenom").value = '';
      document.getElementById("nom").value = '';
    alert("Pas de résultats...");
    }
  }
  // On attend que toutes les personnes soient affichées por égaliser leurs hauteurs
  setTimeout(function(){
    equalHeight($(".person"));
  },1500);
}

// Charge les résultats d'une recherche par structure avec rechercheStructure.php puis les insère dans le div "results"
function updateResultsStructure(){
	document.getElementById("results").innerHTML = '';
  var http = new XMLHttpRequest();
  http.open("POST", "rechercheStructure.php", true);
  http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
  var params = "&structure=" + document.getElementById("structure").value + "&sousstructure=" + document.getElementById("sousStructure").value;
  http.send(params);
  // Une fois le résultat de la requête obtenu, on introduit le code reçu
  // (qui représente toutes les personnes correspondant aux critères)
  // entre les balises du div "results"
  http.onload = function() {
      if(http.responseText != ''){
      	document.getElementById("results").innerHTML = http.responseText;
     	}
     	else{
        document.getElementById("prenom").value = '';
        document.getElementById("nom").value = '';
     		alert("Pas de résultats...");
     	}
  }
  // On attend que toutes les personnes soient affichées por égaliser leurs hauteurs
  setTimeout(function(){
    equalHeight($(".person"));
  },1500);
}

// ----------------------------------------------------- //
// ------------------- Code jQuery --------------------- //
// ----------------------------------------------------- //

// Code jQuery qui permet des actions automatiques à partir du moment où l'ensemble
// du document a été chargé
$(document).ready(function(){
  // Fonction appelée quand le contenu d'une des balises <input> change
  // Débloque le bouton valider si un des champs Nom ou prénom contient plus de 2 caractères
  $('input').keyup(function(){
    if(($('#prenom').val().length >= 2) || ($('#nom').val().length >= 2))
        $("#submitNomPrenom").prop('disabled', false);
    else
        $("#submitNomPrenom").prop('disabled', true);
  });

  // Fonction appelée quand le contenu du menu déroulant "structure" change
  // Débloque le bouton valider et le choix de sous-structure si une structure non nulle est choisie dans le menu déroulant
  $('#structure').change(function(){
    if($('#structure').val() != ''){
        updateSousStructures();
        $("#sousStructure").prop('disabled', false);
        $("#submitStructure").prop('disabled', false);
    }
   else{
        $("#sousStructure").prop('disabled', true);
        $("#submitStructure").prop('disabled', true);
    }
  });

  // Fonction déclenchée lorsque le bouton "submitNomPrenom" est cliqué
  // Lance un "smooth scroll" vers le div des résultats
  $('#submitNomPrenom').click(function() {
    $('html, body').animate({
        scrollTop: $("#results").offset().top
    }, 1000);
  });

  // Fonction déclenchée lorsque le bouton "submitStructure" est cliqué
  // Lance un "smooth scroll" vers le div des résultats
  $('#submitStructure').click(function() {
    $('html, body').animate({
        scrollTop: $("#results").offset().top
    }, 1000);
  });

  // Fonction déclenchée lorsque la fenêtre change de taille
  // Fait appel à la méthode equalHeight() pour égaliser les hauteurs des blocs "person"
  $(window).resize(function() {
		console.log("resize");
		equalHeight($(".person"));
	});
});
