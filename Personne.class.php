<?php
	class Personne{
		private $prenom;
		private $nom;
		private $tel;
		private $tel2;
		private $bureau;
		private $structure;
		private $sousStructure;
		private $poste;
		private $mail;
		private $autorisation;
		private $photo;
		private $login;

		public function __construct($array){
			$this->prenom = $array['prenom'];
			$this->nom = $array['nom']; //contient le nom et le prénom
			$this->tel = $array['tel'];
			$this->tel2 = $array['tel2'];
			$this->bureau = $array['bureau'];
			$this->structure = $array['structure'];
			$this->sousStructure = $array['sousStructure'];
			$this->poste = $array['poste'];
			$this->mail = $array['mail'];
			$this->autorisation = $array['autorisation'];
			$this->login = $array['login'];
			$this->photo = "https://demeter.utc.fr/portal/pls/portal30/portal30.get_photo_utilisateur_mini?username=".$array['login'];
		}

		public function afficher(){
			echo "<div class=\"person large-4 medium-6 small-12 columns end\">";
			echo "<center> <p><img src=".$this->photo." alt=\"Pas d'image\" onError = \"this.onerror=null;this.src='img/people.png';\" /></p> ";
			echo "<p>";
			echo "<b>".$this->nom."</b><br>";
			if($this->tel != '')
				echo "Tel: ".$this->tel."<br>";
			if($this->bureau != '')
				echo "Bureau: ".$this->bureau."<br>";
			echo "<i>".$this->structure."</i><br>";
			if($this->sousStructure != '')
				echo "<i>".$this->sousStructure."</i><br>";
			if($this->poste != '')
				echo $this->poste."<br>";
			echo "<a href=\"mailto:".$this->mail."\">".$this->mail."</a><br>";
			echo "</p>";
			echo "</center>";
			echo "</div>";
		}
	}
?>
