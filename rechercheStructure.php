<?php
	$structure = $_POST['structure'];
	$sousStructure = $_POST['sousstructure'];
	
	include_once('Personne.class.php');


	$query = "https://webapplis.utc.fr/Trombi_ws/mytrombi/resultstruct?pere=".$structure."&fils=".$sousStructure;

	set_error_handler(function() { echo "<p>Webservice indisponible</p>";});
	$json = file_get_contents($query);
	restore_error_handler();

	$res = json_decode($json, true);

	$res_size = count($res);
	if ($res_size != 0) {
		echo "<div id=\"results-content\">";
	
		for($i = 0 ; $i < $res_size ; $i++){
			$Personne = new Personne($res[$i]);
			$Personne->afficher();
		}

		echo "</div>";
	}
?>
